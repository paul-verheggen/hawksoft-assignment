# HawkSoft Assignment README #

Here's what I managed to get done, it took me about 3.5 hours to complete.  I will say that things would have gone a lot faster were my development enviroment set up better in advance, probably spent about an hour trying to get SQL Server / SSMS installed, Visual Studio updated, etc.

### Implemented Features ###

* Allow viewing the list of contacts
* Can edit contact's first name, last name and email
* Can delete contacts

### Missing Features ###

* Pagination (would be necessary for a list of 1000 contacts)
* Adding a new contact
* Displaying address fields and allow them to be editable

### Getting Started ###

Run the `create_database.sql` script on your SQL Server instance, then configure the `HawkSoftAppContext` connection string in `appsettings.json`.  You should be good to go!