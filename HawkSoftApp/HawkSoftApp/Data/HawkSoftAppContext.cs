﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HawkSoftApp.Models;

namespace HawkSoftApp.Data
{
    public class HawkSoftAppContext : DbContext
    {
        public HawkSoftAppContext (DbContextOptions<HawkSoftAppContext> options)
            : base(options)
        {
        }

        public DbSet<HawkSoftApp.Models.Contact> Contact { get; set; }
    }
}
