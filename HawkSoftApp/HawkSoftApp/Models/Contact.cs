﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HawkSoftApp.Models
{
    public class Contact
    {
        public Guid ContactID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Postal { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
    }
}
